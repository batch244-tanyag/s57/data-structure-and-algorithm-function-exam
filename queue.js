let collection = [];

// Write the queue functions below.


// [1] Print queue elements.

function print() {
   return collection;
}

// Enqueue elements
function enqueue(element) {
  collection[collection.length] = element;
  return collection
}

// dequeue elements.
function dequeue() {
   let newCollection = [];

   for (i=0;i<collection.length-1;i++){
      newCollection[i] = collection[i+1];
   }
   return collection = newCollection
}

// get first element

function front() {
    return collection[0];
}

// Get queue size.
function size() {
   return collection.length
}

// Check if queue is not empty
function isEmpty() {
    return (collection.length !==0)? false : true; 
}


module.exports = {
   collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};